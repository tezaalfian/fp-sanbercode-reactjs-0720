import React from "react"
import { BrowserRouter as Router } from "react-router-dom";
import { Container } from "react-bootstrap";
import Header from "./Header";
import Footer from "./Footer";
import Content from "./Content";
import { ContentProvider } from "../contexts/ContentContext";

const Main = () => {
    return (
        <>
            <Router>
                <Header />
                <Container className="content-wrapper">
                    <ContentProvider>
                        <Content />
                    </ContentProvider>
                </Container>
                <Footer />
            </Router>
        </>
    )
}

export default Main;
