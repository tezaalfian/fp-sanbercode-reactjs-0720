import React from "react";
import { ListGroup } from "react-bootstrap";
import { Link } from "react-router-dom";

class Sidebar extends React.Component {
    render() {
        return (
            <ListGroup variant="flush">
                {this.props.menu.map(el => {
                    return (
                        <ListGroup.Item>
                            <Link to={'/' + this.props.parent + '/' + el.toLowerCase().replace(' ', '-')}>{el}</Link>
                        </ListGroup.Item>
                    );
                })}
            </ListGroup>
        );
    }
}

export default Sidebar;