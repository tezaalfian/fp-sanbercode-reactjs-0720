import React from "react";
import { Container } from "react-bootstrap";

class Footer extends React.Component {
    render() {
        return (
            <footer>
                <Container>
                    <span>Copyright © MoviesGames</span>
                    <span>by <strong>Teza Alfian</strong></span>
                </Container>
            </footer>
        );
    }
}

export default Footer;