import React, { useContext, useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Movies from "../pages/movies/Movies";
import Games from "../pages/games/Games";
import Login from "../pages/user/Login";
import Register from "../pages/user/Register";
import Home from "../pages/Home/Home";
import ChangePassword from "../pages/user/ChangePassword";
import axios from "axios";
import { UserContext } from "../contexts/UserContext";
import { ContentContext } from "../contexts/ContentContext";

const Content = () => {
    const [user] = useContext(UserContext);
    const [content, setContent] = useContext(ContentContext);
    useEffect(() => {
        if (content.movies === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies`)
                .then((res) => {
                    // console.log(res.data);
                    setContent({
                        ...content, movies: res.data.map(el => {
                            return {
                                title: el.title,
                                duration: el.duration,
                                description: el.description,
                                genre: el.genre,
                                id: el.id,
                                image_url: el.image_url,
                                rating: el.rating,
                                review: el.review,
                                year: el.year,
                                created_at: el.created_at,
                                updated_at: el.updated_at
                            }
                        })
                    });
                });
        }
        if (content.games === null) {
            axios.get(`https://backendexample.sanbersy.com/api/games`)
                .then((res) => {
                    // console.log(res);
                    setContent({
                        ...content, games: res.data.map(el => {
                            return {
                                id: el.id,
                                name: el.name,
                                genre: el.genre,
                                singlePlayer: el.singlePlayer,
                                multiplayer: el.multiplayer,
                                platform: el.platform,
                                release: el.release,
                                image_url: el.image_url,
                                created_at: el.created_at,
                                updated_at: el.updated_at
                            }
                        })
                    });
                })
        }
    }, [content]);
    return (
        <Switch>
            <Route path="/movies">
                {(user !== null) ? <Movies /> : <Redirect to="/login" />}
            </Route>
            <Route path="/games">
                {(user !== null) ? <Games /> : <Redirect to="/login" />}
            </Route>
            <Route path="/register">
                {(user === null) ? <Register /> : <Redirect to="/" />}
            </Route>
            <Route path="/login">
                {(user === null) ? <Login /> : <Redirect to="/" />}
            </Route>
            <Route path="/change-password">
                {(user !== null) ? <ChangePassword /> : <Redirect to="/login" />}
            </Route>
            <Route exact path="/">
                <Home />
            </Route>
        </Switch>
    );
}

export default Content;