import React from "react";
import { Form, Navbar, Button, Container, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { UserContext } from "../contexts/UserContext";
import { useContext } from "react";

const Header = () => {
    const [user, setUser] = useContext(UserContext);

    const handleLogout = () => {
        setUser(null)
        localStorage.removeItem("user")
    }
    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand>
                    <Link to="/">Home</Link>
                </Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    {user !== null &&
                        <Nav className="mr-auto">
                            <Nav.Link>
                                <Link to="/movies">Movies Editor</Link>
                            </Nav.Link>
                            <Nav.Link href="#link">
                                <Link to="/games">Games Editor</Link>
                            </Nav.Link>
                        </Nav>
                    }
                </Navbar.Collapse>
                <Form inline>
                    {user === null &&
                        <>
                            <Button variant="outline-info">
                                <Link to="/register">Register</Link>
                            </Button>&nbsp;
                            <Button variant="outline-info">
                                <Link to="/login">Login</Link>
                            </Button>
                        </>}
                    {user !== null &&
                        <>
                            <Button variant="outline-info">
                                <Link to="/change-password">Change Password</Link>
                            </Button>&nbsp;
                            <Button variant="outline-info" onClick={handleLogout}>
                                Logout
                            </Button>
                        </>}
                </Form>
            </Container>
        </Navbar>
    );
}

export default Header;