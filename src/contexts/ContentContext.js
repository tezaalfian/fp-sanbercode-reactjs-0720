import React, { useState, createContext } from "react";

export const ContentContext = createContext();

export const ContentProvider = props => {
    const [content, setContent] = useState({
        movies: null,
        games: null
    });

    return (
        <ContentContext.Provider value={[content, setContent]}>
            {props.children}
        </ContentContext.Provider>
    );
};
