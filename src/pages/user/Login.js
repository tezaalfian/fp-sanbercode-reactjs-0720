import React, { useContext, useState, useEffect } from "react"
import { UserContext } from "../../contexts/UserContext";
import { Form, Button, Row, Col } from "react-bootstrap";
import axios from "axios";

const Login = () => {
    const [, setUser] = useContext(UserContext);
    const [input, setInput] = useState({ username: "", password: "" });
    const [listUser, setListUser] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const cekUser = listUser.find(el => el.username === input.username);
        if (input.username.replace(/\s/g, "") !== "" && input.password.replace(/\s/g, "") !== "") {
            if (cekUser === undefined) {
                alert("Akun tidak terdaftar!");
            } else {
                if (cekUser.password === input.password) {
                    setUser({ username: input.username, password: input.password, id: cekUser.id });
                    // console.log(user);
                    localStorage.setItem("user", JSON.stringify(cekUser));
                } else {
                    alert("Password anda Salah!");
                }
                console.log(cekUser);
            }
        } else {
            alert('Tidak Boleh Kosong!');
        }
    }

    const handleChange = (e) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

    useEffect(() => {
        if (listUser === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users`)
                .then((res) => {
                    setListUser(res.data.map(el => {
                        return {
                            username: el.username,
                            password: el.password,
                            id: el.id
                        }
                    }));
                });
        }
    });

    return (
        <>
            <Row>
                <Col md={3}></Col>
                <Col md={6}>
                    <h1 align="center">Login</h1>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" value={input.username} placeholder="Username..." name="username" onChange={handleChange} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={input.password} placeholder="Password" name="password" onChange={handleChange} />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>
                <Col md={3}></Col>
            </Row>
        </>
    )
}

export default Login
