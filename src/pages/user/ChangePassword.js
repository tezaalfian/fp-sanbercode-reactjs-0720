import React, { useState, useContext } from "react"
import { Form, Button, Row, Col } from "react-bootstrap";
import { UserContext } from "../../contexts/UserContext";
import axios from "axios";

const ChangePassword = () => {
    const [user, setUser] = useContext(UserContext);
    const [input, setInput] = useState({ username: user.username, oldpassword: "", newpassword: "" });

    const handleSubmit = (e) => {
        e.preventDefault();
        // console.log(user);
        if (input.oldpassword.replace(/\s/g, "") !== "" && input.newpassword.replace(/\s/g, "") !== "") {
            if (input.oldpassword === user.password) {
                if (input.oldpassword === input.newpassword) {
                    alert('Password lama tidak boleh sama dengan password baru!');
                } else {
                    // console.log(input);
                    axios.put(`https://backendexample.sanbersy.com/api/users/${user.id}`, { password: input.newpassword })
                        .then((res) => {
                            console.log(res.data.password);
                            const newUser = {
                                id: res.data.id,
                                username: res.data.username,
                                password: res.data.password
                            }
                            // console.log(newUser);
                            localStorage.setItem("user", JSON.stringify(newUser));
                            setUser(newUser);
                            alert(`Password berhasil diubah!`);
                            // console.log(user);
                        });
                    setInput({
                        ...input,
                        oldpassword: "",
                        newpassword: ""
                    });
                }
            } else {
                alert('Password lama salah!');
            }
        } else {
            alert('Ada yang masih kosong tuh!');
        }
    }

    const handleChange = (e) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

    return (
        <>
            <Row>
                <Col md={3}></Col>
                <Col md={6}>
                    <h1 align="center">Change Password</h1>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" value={input.username} placeholder="Username..." name="username" onChange={handleChange} readOnly />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Old Password</Form.Label>
                            <Form.Control type="password" value={input.oldpassword} placeholder="Password" name="oldpassword" onChange={handleChange} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>New Password</Form.Label>
                            <Form.Control type="password" value={input.newpassword} placeholder="Password" name="newpassword" onChange={handleChange} />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>
                <Col md={3}></Col>
            </Row>
        </>
    )
}

export default ChangePassword;
