import React, { useState, useEffect } from "react"
import { Form, Button, Row, Col } from "react-bootstrap";
import axios from "axios";

const Register = () => {
    const [input, setInput] = useState({ username: "", password: "", reppassword: "" });
    const [listUser, setListUser] = useState(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const cekUser = listUser.find(el => el.username === input.username);
        if (input.username.replace(/\s/g, "") !== "" && input.password.replace(/\s/g, "") !== "" && input.reppassword.replace(/\s/g, "") !== "") {
            if (input.username.includes(' ')) {
                alert('Username tidak boleh mengandung spasi!');
            } else {
                if (cekUser === undefined) {
                    if (input.password === input.reppassword) {
                        axios.post(`https://backendexample.sanbersy.com/api/users`, {
                            username: input.username,
                            password: input.password
                        })
                            .then((res) => {
                                setListUser(...listUser, {
                                    username: res.data.username,
                                    password: res.data.password
                                });
                                setInput({ username: "", password: "", reppassword: "" });
                                alert('Selamat! akun anda berhasil dibuat! silahkan login');
                            });
                    } else {
                        alert('Password tidak sama!');
                    }
                } else {
                    alert('Username sudah terdaftar!');
                }
            }
        } else {
            alert('Tidak boleh kosong!');
        }
    }

    const handleChange = (e) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

    useEffect(() => {
        if (listUser === null) {
            axios.get(`https://backendexample.sanbersy.com/api/users`)
                .then((res) => {
                    setListUser(res.data.map(el => {
                        return {
                            username: el.username,
                            password: el.password
                        }
                    }));
                });
        }
    });

    return (
        <>
            <Row>
                <Col md={3}></Col>
                <Col md={6}>
                    <h1 align="center">Register</h1>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" value={input.username} placeholder="Username..." name="username" onChange={handleChange} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={input.password} placeholder="Password" name="password" onChange={handleChange} />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Repeat Password</Form.Label>
                            <Form.Control type="password" value={input.reppassword} placeholder="Repeat Password" name="reppassword" onChange={handleChange} />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Col>
                <Col md={3}></Col>
            </Row>
        </>
    )
}

export default Register;
