import React from "react";
import Sidebar from "../../layouts/Sidebar";
import { Row, Col } from "react-bootstrap";
import { useState } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import MoviesTable from "./MoviesTable";
import MoviesForm from "./MoviesForm";

const Movies = () => {
    const [menu] = useState({ parent: "movies", children: ['Table', 'Create'] });
    let { path } = useRouteMatch();
    return (
        <Row>
            <Col md={3}>
                <Sidebar menu={menu.children} parent={menu.parent} />
            </Col>
            <Col md={9}>
                <Switch>
                    <Route exact path={`${path}`}>
                        <h1>Movies for Editor</h1>
                    </Route>
                    <Route path={`${path}/table`}>
                        <MoviesTable />
                    </Route>
                    <Route path={`${path}/create`}>
                        <MoviesForm />
                    </Route>
                    <Route path={`${path}/edit/:idMovie`}>
                        <MoviesForm />
                    </Route>
                </Switch>
            </Col>
        </Row>
    );
}
export default Movies;