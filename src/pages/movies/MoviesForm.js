import React, { useState, useContext } from "react";
import { Form, Col, Button } from "react-bootstrap";
import { ContentContext } from "../../contexts/ContentContext";
import { useParams } from "react-router-dom";
import axios from "axios";
import { useEffect } from "react";

const MoviesForm = () => {
    const [content, setContent] = useContext(ContentContext);
    const [validated, setValidated] = useState(false);
    let { idMovie } = useParams();
    const [input, setInput] = useState({
        title: "",
        genre: "",
        image_url: "",
        year: "",
        duration: "",
        rating: "",
        description: "",
        review: "",
    });

    const dateFormat = () => {
        const date = new Date();
        const time = new Date().toLocaleTimeString('en-GB');
        let year, month, day;
        year = String(date.getFullYear());
        month = String(date.getMonth() + 1);
        if (month.length == 1) {
            month = "0" + month;
        }
        day = String(date.getDate());
        if (day.length == 1) {
            day = "0" + day;
        }
        return String(`${year}-${month}-${day} ${time}`);
    }

    const handleChange = (e) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
            setValidated(true);
        } else {
            if (idMovie === undefined) {
                setInput({ ...input, created_at: dateFormat() });
                axios.post(`https://backendexample.sanbersy.com/api/movies`, input)
                    .then((res) => {
                        // console.log(res.data);
                        setContent({
                            ...content, movies: [...content.movies, {
                                id: res.data.id,
                                title: res.data.title,
                                genre: res.data.genre,
                                image_url: res.data.image_url,
                                year: res.data.year,
                                duration: res.data.duration,
                                rating: res.data.rating,
                                description: res.data.description,
                                review: res.data.review,
                                created_at: res.data.created_at,
                                updated_at: res.data.updated_at,
                            }]
                        });
                        alert('Berhasil ditambahkan!');
                        setInput({
                            title: "",
                            genre: "",
                            image_url: "",
                            year: "",
                            duration: "",
                            rating: "",
                            description: "",
                            review: "",
                        });
                        setValidated(false);
                    });
            } else {
                setInput({ ...input, updated_at: dateFormat() });
                axios.put(`https://backendexample.sanbersy.com/api/movies/${idMovie}`, input)
                    .then((res) => {
                        console.log(res.data);
                        let movie = content.movies.find(el => el.id === res.data.id);
                        movie = {
                            id: res.data.id,
                            title: res.data.title,
                            genre: res.data.genre,
                            image_url: res.data.image_url,
                            year: res.data.year,
                            duration: res.data.duration,
                            rating: res.data.rating,
                            description: res.data.description,
                            review: res.data.review,
                            created_at: res.data.created_at,
                            updated_at: res.data.updated_at,
                        }
                        const oldMovies = content.movies.filter(el => el.id !== res.data.id);
                        setContent({ ...content, movies: [...oldMovies, movie] });
                        alert('Berhasil dirubah!');
                    })
            }
        }
    }

    useEffect(() => {
        if (idMovie !== undefined) {
            axios.get(`https://backendexample.sanbersy.com/api/movies/${parseInt(idMovie)}`)
                .then((res) => {
                    setInput({
                        title: res.data.title,
                        genre: res.data.genre,
                        image_url: res.data.image_url,
                        year: res.data.year,
                        duration: res.data.duration,
                        rating: res.data.rating,
                        description: res.data.description,
                        review: res.data.review
                    });
                })
        } else {
            setInput({
                title: "",
                genre: "",
                image_url: "",
                year: "",
                duration: "",
                rating: "",
                description: "",
                review: "",
            });
        }
    }, [idMovie]);

    return (
        <>
            <h1>Movies Form</h1>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group controlId="formGridEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" name="title" value={input.title} onChange={handleChange} required />
                </Form.Group>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                        <Form.Label>Genre</Form.Label>
                        <Form.Control type="text" name="genre" value={input.genre} onChange={handleChange} required />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridEmail">
                        <Form.Label>Image URL</Form.Label>
                        <Form.Control type="text" name="image_url" value={input.image_url} onChange={handleChange} required />
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                        <Form.Label>Year</Form.Label>
                        <Form.Control type="number" name="year" min={0} value={input.year} onChange={handleChange} required />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                        <Form.Label>Duration</Form.Label>
                        <Form.Control type="number" name="duration" min={0} value={input.duration} onChange={handleChange} required />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                        <Form.Label>Rating</Form.Label>
                        <Form.Control type="number" name="rating" value={input.rating} onChange={handleChange} min={0} max={10} required />
                    </Form.Group>
                </Form.Row>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Decription</Form.Label>
                    <Form.Control as="textarea" name="description" value={input.description} onChange={handleChange} rows="2" required />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Review</Form.Label>
                    <Form.Control as="textarea" name="review" value={input.review} onChange={handleChange} rows="3" required />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </>
    );
}

export default MoviesForm;