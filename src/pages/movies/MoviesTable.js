import React, { useContext, useState } from "react";
import { Table, Button, Accordion, Card, Form, Col, Row } from "react-bootstrap";
import { ContentContext } from "../../contexts/ContentContext";
import { Link } from "react-router-dom";
import axios from "axios";

const MoviesTable = () => {
    const [content, setContent] = useContext(ContentContext);
    const [search, setSearch] = useState("");
    const [filter, setFilter] = useState({
        minYear: "",
        maxYear: "",
        minRat: "",
        maxRat: "",
        minDur: "",
        maxDur: ""
    });

    const [sort, setSort] = useState(null);

    const handleSort = (e) => {
        setSort(e.target.textContent.replace(" ", "_").toLowerCase());
    }

    const handleDelete = (e) => {
        const id = parseInt(e.target.value);
        setContent({ ...content, movies: content.movies.filter(el => el.id !== id) });
        axios.delete(`https://backendexample.sanbersy.com/api/movies/${id}`)
            .then((res) => {
                console.log(res);
                alert(`Berhasil dihapus!`);
            });
    }
    const handleFilter = (e) => {
        setFilter({ ...filter, [e.target.name]: e.target.value });
    }
    const handleSearch = (e) => {
        setSearch(e.target.value);
    }

    const RowMovie = () => {
        let data = [];
        if (content.movies !== null) {
            data = content.movies.map(el => {
                return {
                    id: el.id,
                    title: el.title,
                    genre: el.genre,
                    image_url: el.image_url,
                    year: el.year,
                    duration: el.duration,
                    rating: el.rating,
                    description: el.description,
                    review: el.review,
                    created_at: el.created_at,
                    updated_at: el.updated_at
                }
            });
            if (sort !== null) {
                if ((typeof data[0][sort]) === 'number') {
                    data = data.sort((a, b) => parseFloat(b[sort]) - parseFloat(a[sort]));
                } else if ((typeof data[0][sort]) === 'string') {
                    data = data.sort((a, b) => {
                        const bandA = a[sort].toUpperCase();
                        const bandB = b[sort].toUpperCase();
                        let comparison = 0;
                        if (bandA > bandB) {
                            comparison = 1;
                        } else if (bandA < bandB) {
                            comparison = -1;
                        }
                        return comparison;
                    });
                }
            }
            if (search.replace(/\s/g, "") !== "") {
                data = data.filter(el => {
                    return (el.title.toLowerCase().indexOf(search.toLowerCase()) > -1 || el.genre.toLowerCase().indexOf(search.toLowerCase()) > -1);
                });
            }

            // let { minYear, maxYear, minRat, maxRat, minDur, maxDur } = filter;
            let filterInt = {};
            for (const key in filter) {
                filterInt[key] = parseInt(filter[key]);
            }
            if (filter.minYear !== "" && filter.maxYear !== "") {
                data = data.filter(el => el.year >= filterInt.minYear && el.year <= filterInt.maxYear);
            }
            if (filter.minRat !== "" && filter.maxRat !== "") {
                data = data.filter(el => el.rating >= filterInt.minRat && el.rating <= filterInt.maxRat);
            }
            if (filter.minDur !== "" && filter.maxDur !== "") {
                data = data.filter(el => el.duration >= filterInt.minDur && el.duration <= filterInt.maxDur);
            }
        }
        return data.map((el, i) => {
            return (
                <tr>
                    <td>{i + 1}</td>
                    <td><img src={el.image_url} alt="" width="100" /></td>
                    <td>{el.title}</td>
                    <td>{el.genre}</td>
                    <td>{el.year}</td>
                    <td>{el.duration}</td>
                    <td>{el.rating}</td>
                    <td>{(el.description !== null) ? el.description.substring(0, 30) : el.description}...</td>
                    <td>{(el.review !== null) ? el.review.substring(0, 30) : el.review}...</td>
                    <td>{el.created_at}</td>
                    <td>{el.updated_at}</td>
                    <td>
                        <Button variant="success">
                            <Link to={`edit/${el.id}`}>Edit</Link>
                        </Button>&nbsp;
                        <Button variant="danger" value={el.id} onClick={handleDelete}>Delete</Button>
                    </td>
                </tr>
            );
        });
    }
    return (
        <>
            <h1>Movies Table</h1>
            <Accordion>
                <Card>
                    <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                            Filter
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Form>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Min Year" type="number" value={filter.minYear} name="minYear" onChange={handleFilter} />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Max Year" type="number" value={filter.maxYear} name="maxYear" onChange={handleFilter} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Min Rating" type="number" value={filter.minRat} name="minRat" min={0} max={10} onChange={handleFilter} />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Max Rating" type="number" value={filter.maxRat} name="maxRat" min={0} max={10} onChange={handleFilter} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Min Duration" type="number" value={filter.minDur} name="minDur" onChange={handleFilter} />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Max Duration" type="number" value={filter.maxDur} name="maxDur" onChange={handleFilter} />
                                </Form.Group>
                            </Form.Row>
                        </Form>
                    </Accordion.Collapse>
                    <Card.Header>
                        <Form.Row>
                            <Col sm={4}>
                                <Form.Control size="sm" type="text" placeholder="Search ..." onKeyUp={handleSearch} />
                            </Col>
                        </Form.Row>
                    </Card.Header>
                </Card>
            </Accordion>
            <Table responsive striped bordered hover>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th onClick={handleSort}>Title</th>
                        <th onClick={handleSort}>Genre</th>
                        <th onClick={handleSort}>Year</th>
                        <th onClick={handleSort}>Duration</th>
                        <th onClick={handleSort}>Rating</th>
                        <th>Description</th>
                        <th>Review</th>
                        <th onClick={handleSort}>Created at </th>
                        <th onClick={handleSort}>Updated at </th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <RowMovie />
                </tbody>
            </Table>
        </>
    );
}

export default MoviesTable;