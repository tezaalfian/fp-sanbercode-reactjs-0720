import React, { useState, useContext } from "react"
import { Card, Button, Row, Col, ListGroup, Modal, Image } from "react-bootstrap";
import { ContentContext } from "../../contexts/ContentContext";

const Movies = () => {
    const [content] = useContext(ContentContext);
    const [show, setShow] = useState(false);
    const [movie, setMovie] = useState(null);

    const movieDetail = (e) => {
        setMovie(content.movies.find(el => el.id == e.target.value));
        setShow(true);
    }
    const MovieList = () => {
        return (
            <>
                {content.movies.map(el => {
                    return (
                        <Col lg={3} md={6}>
                            <Card style={{ width: '100%' }}>
                                <Card.Img variant="top" src={el.image_url} />
                                <Card.Body>
                                    <Card.Title>{el.title}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">{el.year}</Card.Subtitle>
                                    <Card.Text>
                                        {(el.description !== null) ? el.description.substring(0, 50) : el.description}...
                                    </Card.Text>
                                    <Button variant="primary" value={el.id} onClick={movieDetail}>Show Detail</Button>
                                </Card.Body>
                            </Card>
                            <br />
                        </Col>
                    );
                })}
            </>
        )
    }

    const ModalMovie = () => {
        return (
            <Modal size="lg" show={show} onHide={() => setShow(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Movie Detail</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col xs={3}>
                            <Image src={movie.image_url} thumbnail />
                        </Col>
                        <Col xs={9}>
                            <h3>{movie.title}</h3>
                            <Card.Subtitle className="mb-2 text-muted">{movie.year}</Card.Subtitle>
                            <ListGroup variant="flush">
                                <ListGroup.Item><strong>Genre :</strong> {movie.genre}</ListGroup.Item>
                                <ListGroup.Item><strong>Durasi :</strong> {movie.duration} menit</ListGroup.Item>
                                <ListGroup.Item><strong>Rating :</strong> {movie.rating} /10</ListGroup.Item>
                                <ListGroup.Item><strong>Deskripsi :</strong> {movie.description}</ListGroup.Item>
                                <ListGroup.Item><strong>Review :</strong> {movie.review}</ListGroup.Item>
                            </ListGroup>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShow(false)}>
                        Close
                            </Button>
                </Modal.Footer>
            </Modal>
        );
    }
    return (
        <>
            <Row>
                <Col><h1>Movies Review</h1></Col>
            </Row>
            <Row>
                {content.movies !== null && <MovieList />}
            </Row>
            {
                movie !== null &&
                <ModalMovie />
            }
        </>
    )
}

export default Movies;
