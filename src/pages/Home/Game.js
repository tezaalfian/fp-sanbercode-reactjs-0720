import React, { useContext, useState } from "react"
import { Card, Row, Col, Modal, Image, Button, ListGroup } from "react-bootstrap";
import { ContentContext } from "../../contexts/ContentContext";

const Game = () => {
    const [content] = useContext(ContentContext);
    const [show, setShow] = useState(false);
    const [game, setGame] = useState(null);

    const gameDetail = (e) => {
        setGame(content.games.find(el => el.id == e.target.value));
        setShow(true);
    };
    const GameList = () => {
        return (
            <>
                {content.games.map(el => {
                    return (
                        <Col lg={3} md={6}>
                            <Card style={{ width: '100%' }}>
                                <Card.Img variant="top" src={el.image_url} />
                                <Card.Body>
                                    <Card.Title>{el.name}</Card.Title>
                                    <Card.Subtitle className="mb-2 text-muted">{el.release}</Card.Subtitle>
                                    <Card.Text>
                                        <p><strong>Genre : </strong>{el.genre}</p>
                                    </Card.Text>
                                    <Button variant="primary" value={el.id} onClick={gameDetail}>Show Detail</Button>
                                </Card.Body>
                            </Card>
                            <br />
                        </Col>
                    );
                })}
            </>
        )
    }
    const ModalGame = () => {
        return (
            <Modal size="lg" show={show} onHide={() => setShow(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Game Detail</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <Col xs={3}>
                            <Image src={game.image_url} thumbnail />
                        </Col>
                        <Col xs={9}>
                            <h3>{game.name}</h3>
                            <Card.Subtitle className="mb-2 text-muted">{game.release}</Card.Subtitle>
                            <ListGroup variant="flush">
                                <ListGroup.Item><strong>Genre :</strong> {game.genre}</ListGroup.Item>
                                <ListGroup.Item><strong>Platform :</strong> {game.platform}</ListGroup.Item>
                                <ListGroup.Item>
                                    <strong>Player : </strong>
                                    {(game.singlePlayer === 1) ? 'Single Player ' : ''}
                                    {(game.multiplayer === 1) ? 'Multi Player' : ''}
                                </ListGroup.Item>
                            </ListGroup>
                        </Col>
                    </Row>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShow(false)}>
                        Close
                            </Button>
                </Modal.Footer>
            </Modal>
        );
    }
    return (
        <>
            <Row>
                <Col><h1>Games</h1></Col>
            </Row>
            <Row>
                {content.games !== null && <GameList />}
            </Row>
            {
                game !== null &&
                <ModalGame />
            }
        </>
    )
}

export default Game;
