import React, { useContext, useState } from "react";
import { Table, Button, Accordion, Card, Form, Col, Row } from "react-bootstrap";
import { ContentContext } from "../../contexts/ContentContext";
import { Link } from "react-router-dom";
import axios from "axios";

const GamesTable = () => {
    const [content, setContent] = useContext(ContentContext);
    const [search, setSearch] = useState("");
    const [filter, setFilter] = useState({
        minYear: "",
        maxYear: "",
        platform: "",
        genre: ""
    });
    const [sort, setSort] = useState(null);

    const handleSort = (e) => {
        setSort(e.target.textContent.replace(" ", "_").toLowerCase());
    }

    const handleDelete = (e) => {
        const id = parseInt(e.target.value);
        setContent({ ...content, games: content.games.filter(el => el.id !== id) });
        axios.delete(`https://backendexample.sanbersy.com/api/games/${id}`)
            .then((res) => {
                console.log(res);
                alert(`Berhasil dihapus!`);
            });
    }
    const handleFilter = (e) => {
        setFilter({ ...filter, [e.target.name]: e.target.value });
    }
    const handleSearch = (e) => {
        setSearch(e.target.value);
    }

    const RowMovie = () => {
        let data = [];
        if (content.games !== null) {
            data = content.games.map(el => {
                return {
                    id: el.id,
                    genre: el.genre,
                    image_url: el.image_url,
                    created_at: el.created_at,
                    updated_at: el.updated_at,
                    name: el.name,
                    singlePlayer: el.singlePlayer,
                    multiplayer: el.multiplayer,
                    platform: el.platform,
                    release: parseInt(el.release),
                }
            });

            if (sort !== null) {
                if ((typeof data[0][sort]) === 'number') {
                    data = data.sort((a, b) => parseFloat(b[sort]) - parseFloat(a[sort]));
                } else if ((typeof data[0][sort]) === 'string') {
                    data = data.sort((a, b) => {
                        const bandA = a[sort].toUpperCase();
                        const bandB = b[sort].toUpperCase();
                        let comparison = 0;
                        if (bandA > bandB) {
                            comparison = 1;
                        } else if (bandA < bandB) {
                            comparison = -1;
                        }
                        return comparison;
                    });
                }
            }
            if (search.replace(/\s/g, "") !== "") {
                data = data.filter(el => {
                    return (el.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
                });
            }

            if (filter.minYear !== "" && filter.maxYear !== "") {
                data = data.filter(el => el.release >= parseInt(filter.minYear) && el.release <= parseInt(filter.maxYear));
            }
            if (filter.platform.replace(/\s/g, "") !== "") {
                data = data.filter(el => {
                    return (el.platform.toLowerCase().indexOf(filter.platform.toLowerCase()) > -1);
                });
            }
            if (filter.genre.replace(/\s/g, "") !== "") {
                data = data.filter(el => {
                    return (el.genre.toLowerCase().indexOf(filter.genre.toLowerCase()) > -1);
                });
            }
        }
        return data.map((el, i) => {
            return (
                <tr>
                    <td>{i + 1}</td>
                    <td><img src={el.image_url} alt="" width="100" /></td>
                    <td>{el.name}</td>
                    <td>{el.genre}</td>
                    <td>{el.release}</td>
                    <td>{el.platform}</td>
                    <td>
                        {el.singlePlayer === 1 ? "Single Player " : ""}
                        {el.multiplayer === 1 ? "Multi Player" : ""}
                    </td>
                    <td>{el.created_at}</td>
                    <td>{el.updated_at}</td>
                    <td>
                        <Button variant="success">
                            <Link to={`edit/${el.id}`}>Edit</Link>
                        </Button>&nbsp;
                        <Button variant="danger" value={el.id} onClick={handleDelete}>Delete</Button>
                    </td>
                </tr>
            );
        });
    }
    return (
        <>
            <h1>Games Table</h1>
            <Accordion>
                <Card>
                    <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                            Filter
                        </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                        <Form>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Min Release Year" type="number" value={filter.minYear} name="minYear" onChange={handleFilter} />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Max Release Year" type="number" value={filter.maxYear} name="maxYear" onChange={handleFilter} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Genre" type="text" value={filter.genre} name="genre" onChange={handleFilter} />
                                </Form.Group>
                                <Form.Group as={Col}>
                                    <Form.Control placeholder="Platform" type="text" value={filter.platform} name="platform" onChange={handleFilter} />
                                </Form.Group>
                            </Form.Row>
                        </Form>
                    </Accordion.Collapse>
                    <Card.Header>
                        <Form.Row>
                            <Col sm={4}>
                                <Form.Control size="sm" type="text" placeholder="Search ..." onKeyUp={handleSearch} />
                            </Col>
                        </Form.Row>
                    </Card.Header>
                </Card>
            </Accordion>
            <Table responsive striped bordered hover>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th onClick={handleSort}>Name</th>
                        <th onClick={handleSort}>Genre</th>
                        <th onClick={handleSort}>Release</th>
                        <th onClick={handleSort}>Platform</th>
                        <th>Player</th>
                        <th onClick={handleSort}>Created At</th>
                        <th onClick={handleSort}>Updated At</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <RowMovie />
                </tbody>
            </Table>
        </>
    );
}

export default GamesTable;