import React, { useState, useContext } from "react";
import { Form, Col, Button } from "react-bootstrap";
import { ContentContext } from "../../contexts/ContentContext";
import { useParams } from "react-router-dom";
import axios from "axios";
import { useEffect } from "react";

const GamesForm = () => {
    const [content, setContent] = useContext(ContentContext);
    const [validated, setValidated] = useState(false);
    let { idGame } = useParams();
    const [input, setInput] = useState({
        name: "",
        genre: "",
        image_url: "",
        release: "",
        platform: "",
        singlePlayer: 0,
        multiplayer: 0
    });

    const handleChange = (e) => {
        setInput({ ...input, [e.target.name]: e.target.value });
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
            setValidated(true);
        } else {
            if (idGame === undefined) {
                axios.post(`https://backendexample.sanbersy.com/api/games`, input)
                    .then((res) => {
                        // console.log(res.data);
                        setContent({
                            ...content, games: [...content.games, {
                                id: res.data.id,
                                name: res.data.name,
                                genre: res.data.genre,
                                image_url: res.data.image_url,
                                release: res.data.release,
                                platform: res.data.platform,
                                singlePlayer: res.data.singlePlayer,
                                multiplayer: res.data.multiplayer,
                                created_at: res.data.created_at,
                                updated_at: res.data.updated_at,
                            }]
                        });
                        alert('Berhasil ditambahkan!');
                        setValidated(false);
                        setInput({
                            name: "",
                            genre: "",
                            image_url: "",
                            release: "",
                            platform: "",
                            singlePlayer: 0,
                            multiplayer: 0
                        });
                    });
            } else {
                axios.put(`https://backendexample.sanbersy.com/api/games/${idGame}`, input)
                    .then((res) => {
                        console.log(res.data);
                        let game = content.games.find(el => el.id === res.data.id);
                        game = {
                            id: res.data.id,
                            name: res.data.name,
                            genre: res.data.genre,
                            image_url: res.data.image_url,
                            release: res.data.release,
                            platform: res.data.platform,
                            singlePlayer: res.data.singlePlayer,
                            multiplayer: res.data.multiplayer,
                            created_at: res.data.created_at,
                            updated_at: res.data.updated_at,
                        }
                        const oldGames = content.games.filter(el => el.id !== res.data.id);
                        setContent({ ...content, games: [...oldGames, game] });
                        alert('Berhasil dirubah!');
                        setValidated(false);
                    })
            }
        }
    }

    useEffect(() => {
        if (idGame !== undefined) {
            axios.get(`https://backendexample.sanbersy.com/api/games/${parseInt(idGame)}`)
                .then((res) => {
                    setInput({
                        name: res.data.name,
                        genre: res.data.genre,
                        image_url: res.data.image_url,
                        release: res.data.release,
                        platform: res.data.platform,
                        singlePlayer: res.data.singlePlayer,
                        multiplayer: res.data.multiplayer
                    });
                })
        } else {
            setInput({
                name: "",
                genre: "",
                image_url: "",
                release: "",
                platform: "",
                singlePlayer: 0,
                multiplayer: 0
            });
        }
    }, [idGame]);

    return (
        <>
            <h1>Games Form</h1>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group controlId="formGridEmail">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" name="name" value={input.name} onChange={handleChange} required />
                </Form.Group>
                <Form.Group controlId="formGridEmail">
                    <Form.Label>Image URL</Form.Label>
                    <Form.Control type="text" name="image_url" value={input.image_url} onChange={handleChange} required />
                </Form.Group>
                <Form.Row>
                    <Form.Group as={Col} controlId="formGridEmail">
                        <Form.Label>Genre</Form.Label>
                        <Form.Control type="text" name="genre" value={input.genre} onChange={handleChange} required />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridEmail">
                        <Form.Label>Release Year</Form.Label>
                        <Form.Control type="number" name="release" min={0} value={input.release} onChange={handleChange} required />
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridPassword">
                        <Form.Label>Platform</Form.Label>
                        <Form.Control type="text" name="platform" value={input.platform} onChange={handleChange} min={0} max={10} required />
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label>Single Player</Form.Label>
                        <Form.Control as="select" name="singlePlayer" value={input.singlePlayer} onChange={handleChange} required>
                            <option value={0}>No</option>
                            <option value={1}>Yes</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group as={Col}>
                        <Form.Label>Multi Player</Form.Label>
                        <Form.Control as="select" name="multiplayer" value={input.multiplayer} onChange={handleChange} required>
                            <option value={0}>No</option>
                            <option value={1}>Yes</option>
                        </Form.Control>
                    </Form.Group>
                </Form.Row>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </>
    );
}

export default GamesForm;