import React from "react";
import Sidebar from "../../layouts/Sidebar";
import { Row, Col } from "react-bootstrap";
import { useState } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import GamesTable from "../games/GamesTable";
import GamesForm from "../games/GamesForm";

const Games = () => {
    const [menu] = useState({ parent: "games", children: ['Table', 'Create'] });
    let { path } = useRouteMatch();
    return (
        <Row>
            <Col md={3}>
                <Sidebar menu={menu.children} parent={menu.parent} />
            </Col>
            <Col md={9}>
                <Switch>
                    <Route exact path={`${path}`}>
                        <h1>Games for Editor</h1>
                    </Route>
                    <Route path={`${path}/table`}>
                        <GamesTable />
                    </Route>
                    <Route path={`${path}/create`}>
                        <GamesForm />
                    </Route>
                    <Route path={`${path}/edit/:idGame`}>
                        <GamesForm />
                    </Route>
                </Switch>
            </Col>
        </Row>
    );
}
export default Games;