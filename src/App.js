import React from 'react';
import Main from "./layouts/Main";
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/style.css';
import { UserProvider } from "./contexts/UserContext";

function App() {
  return (
    <UserProvider>
      <Main />
    </UserProvider>
  );
}

export default App;
